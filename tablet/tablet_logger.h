/** 
 * @file /tablet_vlc_out/tablet/tablet_logger.h
 * @date 2014
 * @copyright (c) ЗАО "Голлард"
 * @author Олег В. Карпов [OlegKarpov]
 * @brief
 */

#ifndef TABLET_LOGGER_H_
#define TABLET_LOGGER_H_

#include <stdio.h>

#if INFO
#define  LOGI(fmt, ...)  fprintf(stderr, "%s:%s:" #fmt,  __FILE__, __FUNCTION__, __VA_ARGS__)
#else
#define  LOGI(fmt, ...)
#endif

#if DEBUG
#define  LOGD(fmt, ...)  fprintf(stderr, "%s:%s:" #fmt,  __FILE__, __FUNCTION__, __VA_ARGS__)
#define LOGCHARS(buf, len) for(int i=0;i<len;i++)fprintf(stderr, "%c", buf[i]);fprintf(stderr, "\n");
#else
#define  LOGD(fmt, ...)
#define LOGCHARS(buf, len)
#define  LOGOBJECT(object, module, name)
#endif

#if WARNING
#define  LOGW(fmt, ...) fprintf(stderr, "%s:%s:" #fmt,  __FILE__, __FUNCTION__, __VA_ARGS__)
#else
#define  LOGW(fmt, ...)
#endif

#if ERROR
#define  LOGE(fmt, ...)  fprintf(stderr, "%s:%s:" #fmt,  __FILE__, __FUNCTION__, __VA_ARGS__)
#else
#define  LOGE(fmt, ...)
#endif

#if TRACE
#define  LOGT(module)  fprintf(stderr, "%s:%s module:%s\n",  __FILE__, __FUNCTION__, module)
#else
#define  LOGT(module)
#endif

#endif /* TABLET_LOGGER_H_ */
