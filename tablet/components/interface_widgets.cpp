/*****************************************************************************
 * interface_widgets.cpp : Custom widgets for the main interface
 ****************************************************************************
 * Copyright (C) 2006-2010 the VideoLAN team
 * $Id: b22fb4396880ab3e45a3236919abbdafa5c1d622 $
 *
 * Authors: Clément Stenac <zorglub@videolan.org>
 *          Jean-Baptiste Kempf <jb@videolan.org>
 *          Rafaël Carré <funman@videolanorg>
 *          Ilkka Ollakka <ileoo@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * ( at your option ) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif


#include "vlc_common.h"
#include "vlc_aout.h"
#include "libvlc2.h"
#include "variables.h"
#include "libvlc_internal.h"
#include "media_player_internal.h"
#include "media_internal.h"
#include <vlc_modules.h>
#include "aout_internal.h"
#include "vout_internal.h"

#include "tablet.hpp"
#include "components/interface_widgets.hpp"
#include "dialogs_provider.hpp"
#include "util/customwidgets.hpp"               // qtEventToVLCKey, QVLCStackedWidget
#include "components/playlist/playlist_model.hpp"               // qtEventToVLCKey, QVLCStackedWidget

#include "menus.hpp"             /* Popup menu on bgWidget */

#include <vlc_vout.h>

#include <QLabel>
#include <QToolButton>
#include <QPalette>
#include <QEvent>
#include <QResizeEvent>
#include <QDate>
#include <QMenu>
#include <QWidgetAction>
#include <QDesktopWidget>
#include <QPainter>
#include <QTimer>
#include <QSlider>
#include <QBitmap>
#include <QUrl>
#include <QDeclarativeProperty>
#include <QSvgRenderer>

#ifdef Q_WS_X11
#   include <X11/Xlib.h>
#   include <qx11info_x11.h>
#endif

#include <math.h>
#include <assert.h>

#define DEBUG 0
#define INFO 0
#define TRACE 0
#include "tablet_logger.h"

#define VIDEO_WIDTH 1024
#define VIDEO_HEIGHT 768

int yuv_to_rgb(picture_t* yuv, QImage* image)
{
	for (int y = 0; y < yuv->format.i_height; y++) {
		for (int x = 0; x < yuv->format.i_width; x++) {
			const int xx = x >> 1;
			const int yy = y >> 1;
			const int Y = yuv->p[0].p_pixels[y * yuv->p[0].i_pitch + x] - 16;
			const int U = yuv->p[1].p_pixels[yy * yuv->p[1].i_pitch + xx] - 128;
			const int V = yuv->p[2].p_pixels[yy * yuv->p[2].i_pitch + xx] - 128;
			const int r = qBound(0, (298 * Y           + 409 * V + 128) >> 8, 255);
			const int g = qBound(0, (298 * Y - 100 * U - 208 * V + 128) >> 8, 255);
			const int b = qBound(0, (298 * Y + 516 * U           + 128) >> 8, 255);

			image->setPixel(x, y, qRgb(r, g, b));
		}
	}

	return 0;
}

/**********************************************************************
 * Video Widget. A simple frame on which video is drawn
 * This class handles resize issues
 **********************************************************************/

std::string getInstallPath()
{
    std::string install_prefix = CONFIGURE_LINE;

    int begin = install_prefix.find("--prefix=");
    if(begin != std::string::npos)
    {
    	begin += 9;
    	int end = install_prefix.find("'", begin);
		if(end != std::string::npos)
		{
				install_prefix = install_prefix.substr(begin, end-begin);
		}

		LOGD("install_prefix_begin:%s\n", install_prefix.c_str());
    }
    else
    {
    	install_prefix = "/usr/local";
    }

    return install_prefix;
}

int print_filters(libvlc_module_description_t* desc)
{
	LOGT(__FILE__);

	LOGD("psz_name:%s\n", desc->psz_name);
	LOGD("psz_shortname:%s\n", desc->psz_shortname);
	LOGD("psz_longname:%s\n", desc->psz_longname);
	LOGD("psz_help:%s\n", desc->psz_help);

	if(desc->p_next)
	{
		print_filters(desc->p_next);
	}

	return 0;
}

VideoWidget::VideoWidget( intf_thread_t *_p_i , BG_TYPE type )
: QFrame( NULL )
, p_intf( _p_i )
, b_expandPixmap( false )
, b_withart( true )
, mBgType(type)
, currentPicture(0)
{
	LOGT(__FILE__);

    vlcPlayer = NULL;
    vlcInstance = libvlc_new(0, NULL);
    libvlc_module_description_t* desc =	libvlc_video_filter_list_get(vlcInstance);
    //    print_filters(desc);

    desc =	libvlc_audio_filter_list_get(vlcInstance);
    print_filters(desc);

    // Allocating the video buffer
    context.pixels = ( uchar* )malloc( ( sizeof( *( context.pixels ) ) * VIDEO_WIDTH * VIDEO_HEIGHT ) * 4 );
    // Allocating the mutex
    context.mutex = new QMutex();

    std::string install_path = QML_PATH;
    std::string top_controls_path = install_path + "/control_widget/top_controls.qml";
    std::string bottom_controls_path = install_path + "/control_widget/bottom_controls.qml";


    mDialogControls = new QWidget(this);
//    mDialogControls->setStyleSheet("background-color:rgba(0, 0, 0, 0);");

    mDialogLayout = new QVBoxLayout(mDialogControls);
    mDialogControls->setLayout(mDialogLayout);
    mCurrentDialog = 0;

//    mCurrentDialog = mDialogControls;

    top_controls = new ControlWidget(_p_i, top_controls_path, this);
    top_controls->setStyleSheet("background-color:rgba(0, 0, 0, 0);");

    bottom_controls = new ControlWidget(_p_i, bottom_controls_path, this);
    bottom_controls->setStyleSheet("background-color:rgba(0, 0, 0, 0);");

    mLayout = new QVBoxLayout(this);
    mLayout->addWidget(top_controls,2);
    mLayout->addWidget(mDialogControls,6);
    mLayout->addWidget(bottom_controls,3);
    mLayout->setMargin(0);
    mLayout->setSpacing(0);
	setLayout(mLayout);

    /* Main action provider */
	QSignalMapper *toolbarActionsMapper = new QSignalMapper( this );
    CONNECT( toolbarActionsMapper, mapped( int ),
             ActionsManager::getInstance( p_intf  ), doAction( int ) );

    bottom_controls->setAction(toolbarActionsMapper, "play_button", PLAY_ACTION);
    bottom_controls->setAction(toolbarActionsMapper, "back_button", PREVIOUS_ACTION);
    bottom_controls->setAction(toolbarActionsMapper, "end_button", NEXT_ACTION);
    bottom_controls->setAction(toolbarActionsMapper, "backward_button", SLOWER_ACTION);
    bottom_controls->setAction(toolbarActionsMapper, "forward_button", FASTER_ACTION);
    bottom_controls->setAction(toolbarActionsMapper, "stop_button", STOP_ACTION);
    bottom_controls->setAction(toolbarActionsMapper, "record_button", RECORD_ACTION);

    top_controls->setAction(toolbarActionsMapper, "settings_button", EXTENDED_ACTION);
    top_controls->setAction(toolbarActionsMapper, "fullscreen_button", FULLSCREEN_ACTION);
    top_controls->setAction(toolbarActionsMapper, "playlist_button", PLAYLIST_ACTION);
    top_controls->setAction(toolbarActionsMapper, "random_button", RANDOM_ACTION);
    top_controls->setLoopAction("loop_button");

	mTimeProgress = bottom_controls->findObject("time_progress");
	if(!mTimeProgress)
	{
		LOGD("%s", "cant find time_progress\n");
	}
	else
	{
		CONNECT( THEMIM->getIM(), positionUpdated( float, int64_t, int ),
				this, setProgressPosition( float, int64_t, int ) );

		CONNECT( mTimeProgress, progressDragged( qreal ),
				 this, progressDragged( qreal ) );
	}

	if(mBgType == kBgEaster)
	{
	    defaultArt = QString( ":/logo/vlc128-xmas.png" );
	}
	else
	{
		defaultArt = QString( ":/logo/vlc128.png" );
	}

    /* A dark background */
    setAutoFillBackground( true );
    QPalette plt = palette();
    plt.setColor( QPalette::Active, QPalette::Window , Qt::black );
    plt.setColor( QPalette::Inactive, QPalette::Window , Qt::black );
    setPalette( plt );


    /* fade in animator */
    setProperty( "opacity", 1.0 );
    fadeAnimation = new QPropertyAnimation( this, "opacity", this );
    fadeAnimation->setDuration( 1000 );
    fadeAnimation->setStartValue( 0.0 );
    fadeAnimation->setEndValue( 1.0 );
    fadeAnimation->setEasingCurve( QEasingCurve::OutSine );
    CONNECT( fadeAnimation, valueChanged( const QVariant & ),
             this, update() );

    CONNECT( THEMIM->getIM(), artChanged( QString ),
             this, updateArt( const QString& ) );


    //easter background
    flakes = new QLinkedList<flake *>();
    i_rate = 2;
    i_speed = 1;
    b_enabled = false;
    easterTimer = new QTimer( this );
    easterTimer->setInterval( 100 );
    CONNECT( easterTimer, timeout(), this, spawnFlakes() );

    if ( isVisible() && b_enabled )
	{
    	easterTimer->start();
	}

    updateArt( "" );

	QTimer *timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(on_timeout()));
	timer->start(200);

    connect( this, SIGNAL( frameReady() ),
             this, SLOT( processNewFrame() ) );

    stable = NULL;
    show();
}

int VideoWidget::slower()
{
	LOGT(__FILE__);

	if(vlcPlayer)
	{
		float rate = libvlc_media_player_get_rate(vlcPlayer);
		LOGD("rate:%f\n", rate);
		libvlc_media_player_set_rate(vlcPlayer, rate*0.9);
	}

	return 0;
}

int VideoWidget::toggle_pause()
{
	LOGT(__FILE__);

	if(mMrl.size() < 1)
	{
		return -1;
	}

	if(vlcPlayer)
	{
		libvlc_media_player_pause ( vlcPlayer );
		repaint();

	}
	else
	{
		videoPlay(mMrl_it->second.c_str());
	}


	return 0;
}

int VideoWidget::toogle_fullscreen(bool f)
{
	LOGT(__FILE__);

	setButtonImage("fullscreen_button", f);
	return 0;
}

int VideoWidget::faster()
{
	LOGT(__FILE__);

	if(vlcPlayer)
	{
		float rate = libvlc_media_player_get_rate(vlcPlayer);
    	LOGD("rate:%f\n", rate);
    	libvlc_media_player_set_rate(vlcPlayer, rate*1.1);
	}

	return 0;
}

int VideoWidget::stop()
{
	LOGT(__FILE__);

	videoStop();
	return 0;
}

int VideoWidget::previous()
{
	videoStop();

	if(mMrl.size() > 0)
	{
		decrementMediaList();

		videoPlay(mMrl_it->second.c_str());
	}

	return 0;
}

int VideoWidget::next()
{
	videoStop();

	if(mMrl.size() > 0)
	{
		incrementMediaList();

		videoPlay(mMrl_it->second.c_str());
	}

	return 0;
}

int VideoWidget::clearPlaylist()
{
	videoStop();
	mMrl.clear();
	mMrl_it = mMrl.end();

	return 0;
}

int VideoWidget::decrementMediaList()
{
	if(mMrl_it == mMrl.begin())
	{
		mMrl_it = mMrl.end();
	}
	mMrl_it--;

	return 0;
}

int VideoWidget::incrementMediaList()
{
	mMrl_it++;
	if(mMrl_it == mMrl.end())
	{
		mMrl_it = mMrl.begin();
	}

	return 0;
}

int VideoWidget::setNextMedia()
{
	LOGT(__FILE__);

	int is_rand = config_GetInt( p_intf, "random");
	int is_loop = config_GetInt( p_intf, "loop" );
	int is_repeat = config_GetInt( p_intf, "repeat" );

	LOGD("is_rand:%d\n", is_rand);

	if(is_loop)
	{
		return 0;
	}
	else if(is_rand)
	{
		int r= rand() % mMrl.size();

		LOGD("rand:%d\n", r);

		std::map<int, std::string>::iterator last = mMrl_it;

		mMrl_it = mMrl.begin();

		int i=0;
		for(;i<r;i++)
		{
			incrementMediaList();
		}

		if(last->first == mMrl_it->first)
		{
			incrementMediaList();
		}
	}
	else
	{
		incrementMediaList();

		if(!is_repeat && mMrl_it == mMrl.begin())
		{
			return -1;
		}
	}

	return 0;
}

void aout_EnableFilter( audio_output_t *p_vout, const char *psz_name,
                        bool b_add, bool b_setconfig )
{
		LOGT(__FILE__);

    char *psz_parser;
    char *psz_string;
    const char *psz_filter_type;

    module_t *p_obj = module_find( psz_name );
    if( !p_obj )
    {
        return;
    }

    LOGD("p_obj:%d\n",  p_obj);

    if( module_provides( p_obj, "audio filter" ) )
    {
        psz_filter_type = "audio-filter";
    }
    else if( module_provides( p_obj, "sub source" ) )
    {
        psz_filter_type = "sub-source";
    }
    else if( module_provides( p_obj, "sub filter" ) )
    {
        psz_filter_type = "sub-filter";
    }
    else
    {
        return;
    }

    psz_string = var_GetString( p_vout, psz_filter_type );

    /* Todo : Use some generic chain manipulation functions */
    if( !psz_string ) psz_string = strdup("");

    LOGD("psz_string:%s\n", psz_string);

    psz_parser = strstr( psz_string, psz_name );
    if( b_add )
    {

    	LOGD("b_add:%s\n", psz_string);

        if( !psz_parser )
        {
            psz_parser = psz_string;
            if( asprintf( &psz_string, (*psz_string) ? "%s:%s" : "%s%s",
                          psz_string, psz_name ) == -1 )
            {
                free( psz_parser );
                return;
            }
            free( psz_parser );
        }
        else
        {
            free( psz_string );
            return;
        }

     	LOGD("b_add2:%s\n", psz_string);

    }
    else
    {
     	LOGD("!b_add:%s\n", psz_string);

        if( psz_parser )
        {
            memmove( psz_parser, psz_parser + strlen(psz_name) +
                            (*(psz_parser + strlen(psz_name)) == ':' ? 1 : 0 ),
                            strlen(psz_parser + strlen(psz_name)) + 1 );

            /* Remove trailing : : */
            if( *(psz_string+strlen(psz_string ) -1 ) == ':' )
            {
                *(psz_string+strlen(psz_string ) -1 ) = '\0';
            }
         }
         else
         {
             free( psz_string );
             return;
         }

       	LOGD("!b_add2:%s\n", psz_string);

    }

    if( b_setconfig )
    {
        config_PutPsz( p_vout, psz_filter_type, psz_string );
    }

  	LOGD("%s-%s\n", psz_filter_type, psz_string);

    var_SetString( p_vout, psz_filter_type, psz_string );

    free( psz_string );
}

int VideoWidget::setVideoFilters()
{
	libvlc_video_set_adjust_int(vlcPlayer, libvlc_adjust_Enable, 1); // enable adjust
	libvlc_video_set_adjust_float( vlcPlayer, 2, 1. );
	float br = libvlc_video_get_adjust_float( vlcPlayer, 2 );

	audio_output_t* a = getAout();
	if(a)
	{
//		aout_EnableFilter( a, "equalizer", 1, false );
//		var_SetFloat( a, "equalizer-preamp", 12.0 );
//		float v = var_GetFloat( a, "equalizer-preamp" );

		aout_EnableFilter( a, "compressor", 1, false );
		var_SetFloat( a, "compressor-attack", 25.0 );
		float v = var_GetFloat( a, "compressor-attack" );

  	LOGD("attack:%f\n", v);
	}

	LOGD("br:%f\n", br);

	return 0;
}

input_thread_t* VideoWidget::getInput()
{
	LOGT(__FILE__);

	input_thread_t* p_input = 0;

	if(vlcPlayer)
	{
		vlc_mutex_lock(&vlcPlayer->object_lock);
		p_input = vlcPlayer->input.p_thread;
		if( p_input )
			vlc_object_hold( p_input );
		else
			libvlc_printerr( "No active input" );
		vlc_mutex_unlock(&vlcPlayer->object_lock);

		LOGD("p_input:%d\n", p_input);
	}

    return p_input;
}

vout_thread_t* VideoWidget::getVout()
{
	LOGT(__FILE__);

	vout_thread_t *p_vout = 0;

	LOGD("vlcPlayer:%d\n", vlcPlayer);

	input_thread_t* p_input = getInput();
	if(p_input)
	{
		p_vout = input_GetVout( p_input );

		LOGD("p_input:%d\n", p_vout);
	}

	return p_vout;
}

audio_output_t* VideoWidget::getAout()
{
	LOGT(__FILE__);

//	playlist_t* p = pl_Get(vlcInstance->p_libvlc_int);
//
//	fprintf(stderr, "%s:%s:playlist:%d\n", __FILE__, __FUNCTION__, p);
//
//	if(p)
//	{
//		audio_output_t* a = playlist_GetAout(p);
//
//		fprintf(stderr, "%s:%s:audio_output_t:%d\n", __FILE__, __FUNCTION__, a);
//
//		return a;
//	}

	audio_output_t *p_aout = 0;

	LOGD("vlcPlayer:%d\n", vlcPlayer);

	if(vlcPlayer)
	{
		input_thread_t* p_input = 0;

	    vlc_mutex_lock(&vlcPlayer->object_lock);
	    p_input = vlcPlayer->input.p_thread;
	    if( p_input )
	        vlc_object_hold( p_input );
	    else
	        libvlc_printerr( "No active input" );
	    vlc_mutex_unlock(&vlcPlayer->object_lock);

	  LOGD("p_input:%d\n", p_input);

		if(p_input)
		{
			input_Control( p_input, INPUT_GET_AOUT, &p_aout );

			LOGD("p_aout:%d\n", p_aout);

		}
	}

    return p_aout;
}

libvlc_int_t *VideoWidget::getInterface()
{
	LOGT(__FILE__);
	LOGD("vlcInstance:%d\n", vlcInstance);
	LOGD("p_libvlc_int:%d\n", vlcInstance->p_libvlc_int);

	return vlcInstance->p_libvlc_int;
}

/* Return the order in which filters should be inserted */
static int FilterOrder( const char *psz_name )
{
    static const struct {
        const char psz_name[10];
        int        i_order;
    } filter[] = {
        { "equalizer",  0 },
    };
    for( unsigned i = 0; i < ARRAY_SIZE(filter); i++ )
    {
        if( !strcmp( filter[i].psz_name, psz_name ) )
            return filter[i].i_order;
    }
    return INT_MAX;
}

bool eee( vlc_object_t *p_obj, vlc_object_t *p_aout,
                              const char *psz_variable,
                              const char *psz_name, bool b_add )
{
	LOGT(__FILE__);

    if( *psz_name == '\0' )
        return false;

    char *psz_list;
    if( p_aout )
    {
        psz_list = var_GetString( p_aout, psz_variable );
    }
    else
    {
        psz_list = var_CreateGetString( p_obj->p_libvlc, psz_variable );
        var_Destroy( p_obj->p_libvlc, psz_variable );
    }

    /* Split the string into an array of filters */
    int i_count = 1;
    for( char *p = psz_list; p && *p; p++ )
        i_count += *p == ':';
    i_count += b_add;

    const char **ppsz_filter = (const char **)calloc( i_count, sizeof(*ppsz_filter) );
    if( !ppsz_filter )
    {
        free( psz_list );
        return false;
    }
    bool b_present = false;
    i_count = 0;
    for( char *p = psz_list; p && *p; )
    {
        char *psz_end = strchr(p, ':');
        if( psz_end )
            *psz_end++ = '\0';
        else
            psz_end = p + strlen(p);
        if( *p )
        {
            b_present |= !strcmp( p, psz_name );
            ppsz_filter[i_count++] = p;
        }
        p = psz_end;
    }
    if( b_present == b_add )
    {
        free( ppsz_filter );
        free( psz_list );
        return false;
    }

    if( b_add )
    {
        int i_order = FilterOrder( psz_name );
        int i;
        for( i = 0; i < i_count; i++ )
        {
            if( FilterOrder( ppsz_filter[i] ) > i_order )
                break;
        }
        if( i < i_count )
            memmove( &ppsz_filter[i+1], &ppsz_filter[i], (i_count - i) * sizeof(*ppsz_filter) );
        ppsz_filter[i] = psz_name;
        i_count++;
    }
    else
    {
        for( int i = 0; i < i_count; i++ )
        {
            if( !strcmp( ppsz_filter[i], psz_name ) )
                ppsz_filter[i] = "";
        }
    }
    size_t i_length = 0;
    for( int i = 0; i < i_count; i++ )
        i_length += 1 + strlen( ppsz_filter[i] );

    char *psz_new = (char *)malloc( i_length + 1 );
    *psz_new = '\0';
    for( int i = 0; i < i_count; i++ )
    {
        if( *ppsz_filter[i] == '\0' )
            continue;
        if( *psz_new )
            strcat( psz_new, ":" );
        strcat( psz_new, ppsz_filter[i] );
    }
    free( ppsz_filter );
    free( psz_list );

    if( p_aout )
        var_SetString( p_aout, psz_variable, psz_new );
    else
        config_PutPsz( p_obj, psz_variable, psz_new );
    free( psz_new );

    return true;
}

int VideoWidget::enableFilter(audio_output_t *p_aout, char* name, bool enabled)
{
	LOGT(__FILE__);	if(!p_aout)
	{
		return -1;
	}

    if (eee (VLC_OBJECT(vlcPlayer), VLC_OBJECT(p_aout),
                                 "audio-filter", name, enabled))
    {
        if (p_aout != NULL)
        {
            aout_owner_t *owner = aout_owner (p_aout);
            atomic_fetch_or (&owner->restart, AOUT_RESTART_FILTERS);
        }
    }

    return 0;
}

libvlc_media_player_t* VideoWidget::getMedia()
{
	return vlcPlayer;
}

void* lock_cb(void *opaque, void **planes)
{
	LOGT(__FILE__);

	VideoWidget* w = (VideoWidget*)opaque;
	*planes = w->context.pixels;

	return 0;
}

void unlock_cb(void *opaque, void *picture, void *const *planes)
{
	LOGT(__FILE__);

	VideoWidget* w = (VideoWidget*)opaque;
	w->takeFrame((unsigned char*)w->context.pixels);
}

static void display(void *opaque, void *picture)
{
	LOGT(__FILE__);

	(void) opaque;
}

int VideoWidget::takeFrame(unsigned char* pixels)
{
	LOGT(__FILE__);

	int h = libvlc_video_get_height( vlcPlayer );
	int w = libvlc_video_get_width( vlcPlayer );
	QImage frame((unsigned char*)context.pixels, VIDEO_WIDTH, VIDEO_HEIGHT, QImage::Format_RGB32);
	currentFrame = frame;

	emit frameReady();
	return 0;
}

void VideoWidget::processNewFrame()
{
	LOGT(__FILE__);

	pthread_t self = pthread_self();

	LOGD("self:%x\n", self);

	repaint();
	return;
}

typedef struct node {
    char         *key;
    struct node  *llink, *rlink;
} node_t;

static int varcmp( const void *a, const void *b )
{
    const variable_t *va = (const variable_t *)a, *vb = (const variable_t *)b;

    /* psz_name must be first */
    assert( va == (const void *)&va->psz_name );
    return strcmp( va->psz_name, vb->psz_name );
}

void *
tprint( const void **vrootp, int (*compar) (const void *, const void *))
{
	node_t * const *rootp = (node_t * const*)vrootp;

	assert(compar != NULL);

	if (rootp == NULL)
		return NULL;

	LOGD("option:%s\n", (*rootp)->key);
	LOGCHARS((*rootp)->key, 10);

	if((*rootp)->llink)
	{
		tprint( (const void **)&(*rootp)->llink, varcmp );
	}

	if((*rootp)->rlink)
	{
		tprint( (const void **)&(*rootp)->rlink, varcmp );
	}

	return NULL;
}

int print_vars2(  vlc_object_t *obj  )
{
    vlc_object_internals_t *priv = vlc_internals( obj );
    vlc_assert_locked( &priv->var_lock );
    tprint( (const void **)&priv->var_root, varcmp );
    return 0;
}

int print_vars( vlc_object_t *p_this )
{
	LOGD("p_this:%d\n", p_this);

	int i_type = 0;

    assert( p_this );

    vlc_object_internals_t *p_priv = vlc_internals( p_this );

    vlc_mutex_lock( &p_priv->var_lock );

    print_vars2( p_this );

    vlc_mutex_unlock( &p_priv->var_lock );

    return i_type;
}

int print_object_hierarchy(vlc_object_t *p_this)
{
    LOGD("object:%s-%s\n", p_this->psz_object_type, p_this->psz_header);

    if(p_this->p_parent)
    {
    	print_object_hierarchy(p_this->p_parent);
    }

	return 0;
}

int VideoWidget::videoPlay(std::string mrl)
{
		LOGT(__FILE__);

    /* Create a new Media */
    libvlc_media_t *vlcMedia = libvlc_media_new_location(vlcInstance, mrl.c_str());
    if (!vlcMedia)
    {
    	LOGE("ailed to open:%s\n", mrl.c_str());
    	return -1;
    }

    libvlc_media_parse(vlcMedia);

    libvlc_media_add_option(vlcMedia, ":adjust=enabled");

    /* Create a new libvlc player */
    vlcPlayer = libvlc_media_player_new_from_media (vlcMedia);

   LOGD("i_options:%d\n", vlcMedia->p_input_item->i_options);

    /* Release the media */
    libvlc_media_release(vlcMedia);


    /* Integrate the video in the interface */
    //libvlc_media_player_set_xwindow(vlcPlayer, winId());
    libvlc_video_set_callbacks(vlcPlayer, lock_cb, unlock_cb, display, this);
    libvlc_video_set_format(vlcPlayer, "RV32", VIDEO_WIDTH, VIDEO_HEIGHT, VIDEO_WIDTH*4);

    /* And start playback */
    libvlc_media_player_play (vlcPlayer);

    vlc_object_t* p_this = VLC_OBJECT(vlcPlayer);
    print_object_hierarchy(p_this);

    char* af = var_GetString( VLC_OBJECT(vlcPlayer), "equalizer" );
    LOGD("equalizer:%s\n", af);

    int t = var_Type(VLC_OBJECT(vlcPlayer), "aout");
    LOGD("var_Type:%x\n", t);

    //print_vars( VLC_OBJECT(vlcPlayer) );

    return 0;
}

int VideoWidget::videoStop()
{
	LOGT(__FILE__);

    /* Stop if something is playing */
    if (vlcPlayer)
    {
        /* stop the media player */
        libvlc_media_player_stop(vlcPlayer);

        /* release the media player */
        libvlc_media_player_release(vlcPlayer);

        /* Reset application values */
        vlcPlayer = NULL;
    }

    return 0;
}

int print_input_item(input_item_t* it)
{
  LOGD("id:%d\n", it->i_id);
  LOGD("psz_name:%s\n", it->psz_name);
  LOGD("psz_uri:%s\n", it->psz_uri);
  LOGD("i_duration:%d\n", it->i_duration);

	return 0;
}

int print_playlist(playlist_t* pl)
{
	LOGT(__FILE__);

	for(int i=0;i<pl->items.i_size;i++)
	{
		print_input_item(pl->items.p_elems[i]->p_input); /**< Arrays of items */
	}

	return 0;
}

int fill_mrl(playlist_t* pl, std::map<int, std::string>& mrl)
{
	LOGT(__FILE__);

	for(int i=0;i<pl->items.i_size;i++)
	{
		input_item_t* it = pl->items.p_elems[i]->p_input;
		if(it)
		{
			mrl[it->i_id] = it->psz_uri;
		}
	}

	return 0;
}

int VideoWidget::addMedia(playlist_t* pl)
{
	LOGT(__FILE__);

	fill_mrl(pl, mMrl);

	if(mMrl.size() < 1)
	{
		return -1;
	}

	mMrl_it = mMrl.begin();

	return 0;
}

int VideoWidget::addMedia(input_item_t *p_item)
{
	LOGT(__FILE__);

	if(!p_item)
	{
		return -1;
	}

	mMrl[p_item->i_id] = p_item->psz_uri;

	if(mMrl.size() < 1)
	{
		return -1;
	}

	mMrl_it = mMrl.find(p_item->i_id);

	return 0;
}

int VideoWidget::startPlay()
{
	LOGT(__FILE__);

	videoStop();
	videoPlay(mMrl_it->second.c_str());

	return 0;
}

void VideoWidget::progressDragged(qreal pos)
{
	LOGT(__FILE__);

	float new_pos = pos;

	LOGD("new position:%f\n", new_pos);

	if(vlcPlayer)
	{
		libvlc_media_player_set_position(vlcPlayer, new_pos );
	}
}

void VideoWidget::setProgressPosition(float pos, int64_t t, int length )
{
	LOGT(__FILE__);

	if(mTimeProgress)
	{
		if(pos != -1.)
		{
			bool dragging = QDeclarativeProperty::read(mTimeProgress, "dragging").toBool();

			LOGD("dragging:%d\n", dragging);

			if(!dragging)
			{
				qreal currentPosition = pos;
				qreal currentLength = t;
				currentLength /= 1000.;
				QDeclarativeProperty::write(mTimeProgress, "currentPosition", currentPosition);
			    QDeclarativeProperty::write(mTimeProgress, "currentLength", currentLength);
			}
		}
	}
}

void VideoWidget::showDialog(QWidget* dialog)
{
	LOGT(__FILE__);

//	if(mLayout)
//	{
//		if(mCurrentDialog)
//		{
//			mLayout->removeWidget(mCurrentDialog);
//			mLayout->insertWidget(1, dialog, 6);
//			mCurrentDialog = dialog;
//			mCurrentDialog->show();
//		}
//		else
//		{
//			fprintf(stderr, "%s:%s:%d:no mCurrentDialog\n", __FILE__, __FUNCTION__, __LINE__);
//		}
//	}
//	else
//	{
//		fprintf(stderr, "%s:%s:%d:no mLayout\n", __FILE__, __FUNCTION__, __LINE__);
//	}

	if(mDialogLayout)
	{
		if(mCurrentDialog)
		{
			mCurrentDialog->hide();
			mDialogLayout->removeWidget(mCurrentDialog);
		}
		else
		{
			LOGE("%s", "no mCurrentDialog\n");
		}

		mDialogLayout->insertWidget(1, dialog, 6);
		mCurrentDialog = dialog;
		mCurrentDialog->show();
	}
	else
	{
		LOGE("%s", "no mDialogLayout\n");
	}
}

void VideoWidget::hideDialog()
{
	LOGT(__FILE__);

//	if(mLayout)
//	{
//		if(mCurrentDialog)
//		{
//			mCurrentDialog->hide();
//			mLayout->removeWidget(mCurrentDialog);
//			mLayout->insertWidget(1, mDialogControls, 6);
//			mCurrentDialog = mDialogControls;
//		}
//		else
//		{
//			fprintf(stderr, "%s:%s:%d:no mCurrentDialog\n", __FILE__, __FUNCTION__, __LINE__);
//		}
//	}
//	else
//	{
//		fprintf(stderr, "%s:%s:%d:no mLayout\n", __FILE__, __FUNCTION__, __LINE__);
//	}

	if(mDialogLayout)
	{
		if(mCurrentDialog)
		{
			mCurrentDialog->hide();
			mDialogLayout->removeWidget(mCurrentDialog);
			mCurrentDialog = 0;
		}
		else
		{
			LOGE("%s", "no mCurrentDialog\n");
		}
	}
	else
	{
		LOGE("%s", "no mDialogLayout\n");
	}
}

int VideoWidget::setButtonImage(std::string button, int imageIndex)
{
	LOGT(__FILE__);

	QObject* but = bottom_controls->findObject(button.c_str());
	if(!but)
	{
		but = top_controls->findObject(button.c_str());
	}

	if(but)
	{
		LOGD("QDeclarativeProperty:%d\n", imageIndex);

		QDeclarativeProperty::write(but, "currentState", imageIndex);
	}
	else
	{
		LOGE("can find button:%s\n", button.c_str());
	}

	return 0;
}

int paint_ev = 0;
int VideoWidget::paintFrame( QPaintEvent *e )
{
	LOGT(__FILE__);

	LOGD("paint_ev:%d\n", paint_ev);

	paint_ev++;

	input_thread_t * ith = MainInputManager::getInstance(p_intf)->getInput();
	if(ith)
	{
		vout_thread_t *p_vout = input_GetVout(ith);
		if(p_vout)
		{
//			picture_t* pic = vout_GetPicture(p_vout);
//			if(pic)
//			{
//				if(pic)
//				{
//					fprintf(stderr, "pic:%d\n", pic->i_planes);
//
//					QImage frame(pic->format.i_width, pic->format.i_height, QImage::Format_RGB32);
//					int res = yuv_to_rgb(pic, &frame);
//					currentFrame = frame;
//				}
//
//				vout_ReleasePicture(p_vout, pic);
//
//			}

			if(p_vout->p)
			{
				vlc_mutex_trylock(&p_vout->p->snapshot.lock);
				LOGD("snapshot:%d\n", p_vout->p->snapshot.picture);

				p_vout->p->snapshot.request_count = 1;
				if(p_vout->p->snapshot.picture)
				{
					QImage frame(p_vout->p->snapshot.picture->format.i_width, p_vout->p->snapshot.picture->format.i_height, QImage::Format_RGB32);
					int res = yuv_to_rgb(p_vout->p->snapshot.picture, &frame);
					currentFrame = frame;
				}

				vlc_mutex_unlock(&p_vout->p->snapshot.lock);



//				picture_t* fil = p_vout->p->display.filtered;
//				fprintf(stderr, "filtered:%d\n", fil);
//
//				picture_t* pic = p_vout->p->displayed.current;
//#if DEBUG_PAINT
//				fprintf(stderr, "decoded:%d\n", pic);
//#endif
//
//				if(pic && pic!= currentPicture)
//				{
//				    vlc_mutex_lock(&p_vout->p->picture_lock);
//
//					currentPicture = pic;
//#if DEBUG_PAINT
//					fprintf(stderr, "currentPicture:%d\n", currentPicture);
//#endif
//
//					QImage frame(currentPicture->format.i_width, currentPicture->format.i_height, QImage::Format_RGB32);
//					int res = yuv_to_rgb(currentPicture, &frame);
//					currentFrame = frame;
//
//					vlc_mutex_unlock(&p_vout->p->picture_lock);
//
//				}
//				else
//				{
//#if DEBUG_PAINT
//					fprintf(stderr, "no current picture\n");
//#endif
//
//				}
			}
			else
			{
				LOGE("%s", "no p_vout->p\n");
			}
		}
		else
		{
			LOGE("%s", "no p_vout\n");
		}
	}
	else
	{
		LOGE("%s", "no input\n");
	}

	QPainter painter(this);
	painter.drawImage(QPoint(0,0), currentFrame.scaled(size()));
	//painter.drawImage(QPoint(0,0), currentFrame.scaled(300,300));

	return 0;
}

int VideoWidget::paintVMEMFrame( QPaintEvent *e )
{
	LOGT(__FILE__);

	pthread_t self = pthread_self();

	LOGD("self:%x\n", __FILE__, __FUNCTION__, self)

	mTimer.Update();

    mFps++;

    if(mTimer.GetElapsed() > 1.0)
    {

	LOGD("mFps:%f\n", mFps);
	LOGD("fps:%f\n", libvlc_media_player_get_fps( vlcPlayer ));

		mFps = 0;
		mTimer.Reset();
    }

	QPainter painter(this);


	libvlc_time_t ct = libvlc_media_player_get_time( vlcPlayer );
	libvlc_time_t l = libvlc_media_player_get_length( vlcPlayer );
	float pos = libvlc_media_player_get_position( vlcPlayer );

	LOGD("%d-%d-%f\n", ct, l, pos);

	setProgressPosition(pos, ct, l);

	QPoint p(0,0);
	painter.drawImage(p, currentFrame.scaled(size()));

    return 0;
}

int VideoWidget::paintBackground( QPaintEvent *e )
{
	LOGT(__FILE__);

	if ( b_withart )
	{
		int i_maxwidth, i_maxheight;
		QPixmap pixmap = QPixmap( pixmapUrl );
		QPainter painter(this);
		QBitmap pMask;
		float f_alpha = 1.0;

		i_maxwidth  = __MIN( maximumWidth(), width() ) - MARGIN * 2;
		i_maxheight = __MIN( maximumHeight(), height() ) - MARGIN * 2;

		painter.setOpacity( property( "opacity" ).toFloat() );

		if ( height() > MARGIN * 2 )
		{
			/* Scale down the pixmap if the widget is too small */
			if( pixmap.width() > i_maxwidth || pixmap.height() > i_maxheight )
			{
				pixmap = pixmap.scaled( i_maxwidth, i_maxheight,
								Qt::KeepAspectRatio, Qt::SmoothTransformation );
			}
			else
			if ( b_expandPixmap &&
				 pixmap.width() < width() && pixmap.height() < height() )
			{
				/* Scale up the pixmap to fill widget's size */
				f_alpha = ( (float) pixmap.height() / (float) height() );
				pixmap = pixmap.scaled(
						width() - MARGIN * 2,
						height() - MARGIN * 2,
						Qt::KeepAspectRatio,
						( f_alpha < .2 )? /* Don't waste cpu when not visible */
							Qt::SmoothTransformation:
							Qt::FastTransformation
						);
				/* Non agressive alpha compositing when sizing up */
				pMask = QBitmap( pixmap.width(), pixmap.height() );
				pMask.fill( QColor::fromRgbF( 1.0, 1.0, 1.0, f_alpha ) );
				pixmap.setMask( pMask );
			}

			painter.drawPixmap(
					MARGIN + ( i_maxwidth - pixmap.width() ) /2,
					MARGIN + ( i_maxheight - pixmap.height() ) /2,
					pixmap);
		}
	}

	return 0;
}

int VideoWidget::paintEasterBackground( QPaintEvent *e )
{
	LOGT(__FILE__);

    QPainter painter(this);

    painter.setBrush( QBrush( QColor(Qt::white) ) );
    painter.setPen( QPen(Qt::white) );

    QLinkedList<flake *>::const_iterator it = flakes->constBegin();
    while( it != flakes->constEnd() )
    {
        const flake * const f = *(it++);
        if ( f->b_fat )
        {
            /* Xsnow like :p */
            painter.drawPoint( f->point.x(), f->point.y() -1 );
            painter.drawPoint( f->point.x() + 1, f->point.y() );
            painter.drawPoint( f->point.x(), f->point.y() +1 );
            painter.drawPoint( f->point.x() - 1, f->point.y() );
        }
        else
        {
            painter.drawPoint( f->point );
        }
    }

    paintBackground( e );

    return 0;
}

void VideoWidget::paintEvent(QPaintEvent * e)
{
	LOGT(__FILE__);

	if(vlcPlayer)
	{
		paintVMEMFrame( e );
	}
	else
	{
		switch(mBgType)
		{
		case kBgDefault:
			paintBackground( e );
			break;

		case kBgEaster:
			paintEasterBackground( e );
			break;
		}
	}

	QWidget::paintEvent( e);
}

void VideoWidget::on_timeout()
{
	LOGT(__FILE__);

	if(vlcPlayer)
	{
		int state = libvlc_media_player_get_state( vlcPlayer );

		LOGD("state:%d\n", state);

		if(state == libvlc_Playing || state == libvlc_Paused)
		{
			setButtonImage("play_button", state == libvlc_Playing);
		}
		else if(state == libvlc_Ended)
		{
			setButtonImage("play_button", 0);

			if(!setNextMedia())
			{
				startPlay();
			}
			else
			{
				videoStop();
			}
		}
		else if(state == libvlc_Stopped)
		{
			setButtonImage("play_button", 0);
		}
	}

    int is_rand = config_GetInt( p_intf, "random");
	setButtonImage("random_button", is_rand);

	int is_loop = config_GetInt( p_intf, "loop" );
	int is_repeat = config_GetInt( p_intf, "repeat" );

	LOGD("is_loop:%d\n", is_loop);
	LOGD("is_repeat:%d\n", is_repeat);

	int st = 0;
	if(is_loop)
	{
		st = 1;
	}
	else if(is_repeat)
	{
		st = 2;
	}

	setButtonImage("loop_button", st);
}

VideoWidget::~VideoWidget()
{
	LOGT(__FILE__);

	easterTimer->stop();
    delete easterTimer;
    reset();
    delete flakes;

    /* Ensure we are not leaking the video output. This would crash. */
    assert( !stable );
}

void VideoWidget::sync( void )
{
	LOGT(__FILE__);

#ifdef Q_WS_X11
    /* Make sure the X server has processed all requests.
     * This protects other threads using distinct connections from getting
     * the video widget window in an inconsistent states. */
    XSync( QX11Info::display(), False );
#endif
}

/**
 * Request the video to avoid the conflicts
 **/
WId VideoWidget::request( int *pi_x, int *pi_y,
                          unsigned int *pi_width, unsigned int *pi_height,
                          bool b_keep_size )
{
	LOGT(__FILE__);

    msg_Dbg( p_intf, "Video was requested %i, %i", *pi_x, *pi_y );

    if( stable )
    {
        msg_Dbg( p_intf, "embedded video already in use" );
        return 0;
    }
    if( b_keep_size )
    {
        *pi_width  = size().width();
        *pi_height = size().height();
    }

    /* The owner of the video window needs a stable handle (WinId). Reparenting
     * in Qt4-X11 changes the WinId of the widget, so we need to create another
     * dummy widget that stays within the reparentable widget. */
    stable = new QWidget(this);
    QPalette plt = palette();
    plt.setColor( QPalette::Window, Qt::black );
    stable->setPalette( plt );
    stable->setAutoFillBackground(true);
    //stable->resize(0,0);
    /* Force the widget to be native so that it gets a winId() */
    //stable->setAttribute( Qt::WA_NativeWindow, true );
    /* Indicates that the widget wants to draw directly onto the screen.
       Widgets with this attribute set do not participate in composition
       management */
    /* This is currently disabled on X11 as it does not seem to improve
     * performance, but causes the video widget to be transparent... */
#if !defined (Q_WS_X11) && !defined (Q_WS_QPA)
    stable->setAttribute( Qt::WA_PaintOnScreen, true );
#endif

    XWindowAttributes a;

#ifdef Q_WS_X11
    /* HACK: Only one X11 client can subscribe to mouse button press events.
     * VLC currently handles those in the video display.
     * Force Qt4 to unsubscribe from mouse press and release events. */
    Display *dpy = QX11Info::display();
    Window w = stable->winId();
    XWindowAttributes attr;

    XGetWindowAttributes( dpy, w, &attr );
    attr.your_event_mask &= ~(ButtonPressMask|ButtonReleaseMask);
    XSelectInput( dpy, w, attr.your_event_mask );
#endif
    sync();

    return stable->winId();
}

/* Set the Widget to the correct Size */
/* Function has to be called by the parent
   Parent has to care about resizing itself */
void VideoWidget::SetSizing( unsigned int w, unsigned int h )
{
	LOGT(__FILE__);

    resize( w, h );
    emit sizeChanged( w, h );
    /* Work-around a bug?misconception? that would happen when vout core resize
       twice to the same size and would make the vout not centered.
       This cause a small flicker.
       See #3621
     */
    if( (unsigned)size().width() == w && (unsigned)size().height() == h )
        updateGeometry();
    sync();
}

void VideoWidget::release( void )
{
	LOGT(__FILE__);

    msg_Dbg( p_intf, "Video is not needed anymore" );

    if( stable )
    {
        stable->deleteLater();
        stable = NULL;
    }

    updateGeometry();
}

void VideoWidget::updateArt( const QString& url )
{
	LOGT(__FILE__);

    if ( !url.isEmpty() )
        pixmapUrl = url;
    else
        pixmapUrl = defaultArt;
    update();
}

void VideoWidget::showBackground( QShowEvent * e )
{
    Q_UNUSED( e );

    if ( b_withart )
	{
    	fadeAnimation->start();
	}
}

void VideoWidget::showEasterBackground( QShowEvent * e )
{
    if ( b_enabled )
	{
    	easterTimer->start();
	}

    showBackground( e );
}

void VideoWidget::showEvent( QShowEvent * e )
{
	LOGT(__FILE__);

	switch(mBgType)
	{
	case kBgDefault:
		showBackground( e );
		break;

	case kBgEaster:
		showEasterBackground( e );
		break;
	}
}

void VideoWidget::contextMenuEvent( QContextMenuEvent *event )
{
	LOGT(__FILE__);

    VLCMenuBar::PopupMenu( p_intf, true );
    event->accept();
}

void VideoWidget::animate()
{
	LOGT(__FILE__);

    b_enabled = true;
    if ( isVisible() )
	{
    	easterTimer->start();
	}
}

void VideoWidget::hideEvent( QHideEvent *e )
{
	LOGT(__FILE__);

	easterTimer->stop();
    reset();
}

void VideoWidget::resizeEvent( QResizeEvent *e )
{
	LOGT(__FILE__);

    reset();
}

void VideoWidget::mouseReleaseEvent( QMouseEvent * event )
{
	LOGT(__FILE__);

	if(!mCurrentDialog)
	{
		QObject* root = top_controls->rootObject();
		if(root)
		{
			QVariant returnedValue;
			QMetaObject::invokeMethod(root, "switchState", Q_RETURN_ARG(QVariant, returnedValue));
		}
		else
		{
			LOGE("%s", "no top_controls root\n");
		}

		root = bottom_controls->rootObject();
		if(root)
		{
			QVariant returnedValue;
			QMetaObject::invokeMethod(root, "switchState", Q_RETURN_ARG(QVariant, returnedValue));
		}
		else
		{
			LOGE("%s", "no top_controls root\n");
		}
	}
}

void VideoWidget::spawnFlakes()
{
	LOGT(__FILE__);

    if ( ! isVisible() ) return;

    double w = (double) width() / RAND_MAX;

    int i_spawn = ( (double) qrand() / RAND_MAX ) * i_rate;

    QLinkedList<flake *>::iterator it = flakes->begin();
    while( it != flakes->end() )
    {
        flake *current = *it;
        current->point.setY( current->point.y() + i_speed );
        if ( current->point.y() + i_speed >= height() )
        {
            delete current;
            it = flakes->erase( it );
        }
        else
            it++;
    }

    if ( flakes->size() < MAX_FLAKES )
    for ( int i=0; i<i_spawn; i++ )
    {
        flake *f = new flake;
        f->point.setX( qrand() * w );
        f->b_fat = ( qrand() < ( RAND_MAX * .33 ) );
        flakes->append( f );
    }
    update();
}

void VideoWidget::reset()
{
	LOGT(__FILE__);

    while ( !flakes->isEmpty() )
    {
        delete flakes->takeFirst();
    }
}

#if 0
#include <QPushButton>
#include <QHBoxLayout>

/**********************************************************************
 * Visualization selector panel
 **********************************************************************/
VisualSelector::VisualSelector( intf_thread_t *_p_i ) :
                                QFrame( NULL ), p_intf( _p_i )
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    QHBoxLayout *layout = new QHBoxLayout( this );
    layout->setMargin( 0 );
    QPushButton *prevButton = new QPushButton( "Prev" );
    QPushButton *nextButton = new QPushButton( "Next" );
    layout->addWidget( prevButton );
    layout->addWidget( nextButton );

    layout->addStretch( 10 );
    layout->addWidget( new QLabel( qtr( "Current visualization" ) ) );

    current = new QLabel( qtr( "None" ) );
    layout->addWidget( current );

    BUTTONACT( prevButton, prev() );
    BUTTONACT( nextButton, next() );

    setLayout( layout );
    setMaximumHeight( 35 );
}

VisualSelector::~VisualSelector()
{}

void VisualSelector::prev()
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    char *psz_new = aout_VisualPrev( p_intf );
    if( psz_new )
    {
        current->setText( qfu( psz_new ) );
        free( psz_new );
    }
}

void VisualSelector::next()
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    char *psz_new = aout_VisualNext( p_intf );
    if( psz_new )
    {
        current->setText( qfu( psz_new ) );
        free( psz_new );
    }
}
#endif

SpeedLabel::SpeedLabel( intf_thread_t *_p_intf, QWidget *parent )
           : QLabel( parent ), p_intf( _p_intf )
{
	LOGT(__FILE__);

    tooltipStringPattern = qtr( "Current playback speed: %1\nClick to adjust" );

    /* Create the Speed Control Widget */
    speedControl = new SpeedControlWidget( p_intf, this );
    speedControlMenu = new QMenu( this );

    QWidgetAction *widgetAction = new QWidgetAction( speedControl );
    widgetAction->setDefaultWidget( speedControl );
    speedControlMenu->addAction( widgetAction );

    /* Change the SpeedRate in the Label */
    CONNECT( THEMIM->getIM(), rateChanged( float ), this, setRate( float ) );

    DCONNECT( THEMIM, inputChanged( input_thread_t * ),
              speedControl, activateOnState() );

    setFrameStyle( QFrame::StyledPanel | QFrame::Raised );
    setLineWidth( 1 );

    setRate( var_InheritFloat( THEPL, "rate" ) );
}

SpeedLabel::~SpeedLabel()
{
	LOGT(__FILE__);

    delete speedControl;
    delete speedControlMenu;
}

/****************************************************************************
 * Small right-click menu for rate control
 ****************************************************************************/

void SpeedLabel::showSpeedMenu( QPoint pos )
{
	LOGT(__FILE__);

    speedControlMenu->exec( QCursor::pos() - pos
                            + QPoint( -70 + width()/2, height() ) );
}

void SpeedLabel::setRate( float rate )
{
	LOGT(__FILE__);

    QString str;
    str.setNum( rate, 'f', 2 );
    str.append( "x" );
    setText( str );
    setToolTip( tooltipStringPattern.arg( str ) );
    speedControl->updateControls( rate );
}

/**********************************************************************
 * Speed control widget
 **********************************************************************/
SpeedControlWidget::SpeedControlWidget( intf_thread_t *_p_i, QWidget *_parent )
                    : QFrame( _parent ), p_intf( _p_i )
{
	LOGT(__FILE__);

    QSizePolicy sizePolicy( QSizePolicy::Fixed, QSizePolicy::Maximum );
    sizePolicy.setHorizontalStretch( 0 );
    sizePolicy.setVerticalStretch( 0 );

    speedSlider = new QSlider( this );
    speedSlider->setSizePolicy( sizePolicy );
    speedSlider->setMinimumSize( QSize( 140, 20 ) );
    speedSlider->setOrientation( Qt::Horizontal );
    speedSlider->setTickPosition( QSlider::TicksBelow );

    speedSlider->setRange( -34, 34 );
    speedSlider->setSingleStep( 1 );
    speedSlider->setPageStep( 1 );
    speedSlider->setTickInterval( 17 );

    CONNECT( speedSlider, valueChanged( int ), this, updateRate( int ) );

    QToolButton *normalSpeedButton = new QToolButton( this );
    normalSpeedButton->setMaximumSize( QSize( 26, 16 ) );
    normalSpeedButton->setAutoRaise( true );
    normalSpeedButton->setText( "1x" );
    normalSpeedButton->setToolTip( qtr( "Revert to normal play speed" ) );

    CONNECT( normalSpeedButton, clicked(), this, resetRate() );

    QToolButton *slowerButton = new QToolButton( this );
    slowerButton->setMaximumSize( QSize( 26, 16 ) );
    slowerButton->setAutoRaise( true );
    slowerButton->setToolTip( tooltipL[SLOWER_BUTTON] );
    slowerButton->setIcon( QIcon( iconL[SLOWER_BUTTON] ) );
    CONNECT( slowerButton, clicked(), THEMIM->getIM(), slower() );

    QToolButton *fasterButton = new QToolButton( this );
    fasterButton->setMaximumSize( QSize( 26, 16 ) );
    fasterButton->setAutoRaise( true );
    fasterButton->setToolTip( tooltipL[FASTER_BUTTON] );
    fasterButton->setIcon( QIcon( iconL[FASTER_BUTTON] ) );
    CONNECT( fasterButton, clicked(), THEMIM->getIM(), faster() );

/*    spinBox = new QDoubleSpinBox();
    spinBox->setDecimals( 2 );
    spinBox->setMaximum( 32 );
    spinBox->setMinimum( 0.03F );
    spinBox->setSingleStep( 0.10F );
    spinBox->setAlignment( Qt::AlignRight );

    CONNECT( spinBox, valueChanged( double ), this, updateSpinBoxRate( double ) ); */

    QGridLayout* speedControlLayout = new QGridLayout( this );
    speedControlLayout->addWidget( speedSlider, 0, 0, 1, 3 );
    speedControlLayout->addWidget( slowerButton, 1, 0 );
    speedControlLayout->addWidget( normalSpeedButton, 1, 1, 1, 1, Qt::AlignRight );
    speedControlLayout->addWidget( fasterButton, 1, 2, 1, 1, Qt::AlignRight );
    //speedControlLayout->addWidget( spinBox );
    speedControlLayout->setContentsMargins( 0, 0, 0, 0 );
    speedControlLayout->setSpacing( 0 );

    lastValue = 0;

    activateOnState();
}

void SpeedControlWidget::activateOnState()
{
	LOGT(__FILE__);

    speedSlider->setEnabled( THEMIM->getIM()->hasInput() );
    //spinBox->setEnabled( THEMIM->getIM()->hasInput() );
}

void SpeedControlWidget::updateControls( float rate )
{
	LOGT(__FILE__);

    if( speedSlider->isSliderDown() )
    {
        //We don't want to change anything if the user is using the slider
        return;
    }

    double value = 17 * log( rate ) / log( 2. );
    int sliderValue = (int) ( ( value > 0 ) ? value + .5 : value - .5 );

    if( sliderValue < speedSlider->minimum() )
    {
        sliderValue = speedSlider->minimum();
    }
    else if( sliderValue > speedSlider->maximum() )
    {
        sliderValue = speedSlider->maximum();
    }
    lastValue = sliderValue;

    speedSlider->setValue( sliderValue );
    //spinBox->setValue( rate );
}

void SpeedControlWidget::updateRate( int sliderValue )
{
	LOGT(__FILE__);

    if( sliderValue == lastValue )
        return;

    double speed = pow( 2, (double)sliderValue / 17 );
    int rate = INPUT_RATE_DEFAULT / speed;

    THEMIM->getIM()->setRate(rate);
    //spinBox->setValue( var_InheritFloat( THEPL, "rate" ) );
}

void SpeedControlWidget::updateSpinBoxRate( double r )
{
	LOGT(__FILE__);

    var_SetFloat( THEPL, "rate", r );
}

void SpeedControlWidget::resetRate()
{
	LOGT(__FILE__);

    THEMIM->getIM()->setRate( INPUT_RATE_DEFAULT );
}

CoverArtLabel::CoverArtLabel( QWidget *parent, intf_thread_t *_p_i )
    : QLabel( parent ), p_intf( _p_i ), p_item( NULL )
{
	LOGT(__FILE__);

    setContextMenuPolicy( Qt::ActionsContextMenu );
    CONNECT( THEMIM->getIM(), artChanged( input_item_t * ),
             this, showArtUpdate( input_item_t * ) );

    setMinimumHeight( 128 );
    setMinimumWidth( 128 );
    setScaledContents( false );
    setAlignment( Qt::AlignCenter );

    QAction *action = new QAction( qtr( "Download cover art" ), this );
    CONNECT( action, triggered(), this, askForUpdate() );
    addAction( action );

    action = new QAction( qtr( "Add cover art from file" ), this );
    CONNECT( action, triggered(), this, setArtFromFile() );
    addAction( action );

    p_item = THEMIM->currentInputItem();
    if( p_item )
    {
        vlc_gc_incref( p_item );
        showArtUpdate( p_item );
    }
    else
        showArtUpdate( "" );
}

CoverArtLabel::~CoverArtLabel()
{
	LOGT(__FILE__);

    QList< QAction* > artActions = actions();
    foreach( QAction *act, artActions )
        removeAction( act );
    if ( p_item ) vlc_gc_decref( p_item );
}

void CoverArtLabel::setItem( input_item_t *_p_item )
{
	LOGT(__FILE__);

    if ( p_item ) vlc_gc_decref( p_item );
    p_item = _p_item;
    if ( p_item ) vlc_gc_incref( p_item );
}

void CoverArtLabel::showArtUpdate( const QString& url )
{
	LOGT(__FILE__);

    QPixmap pix;
    if( !url.isEmpty() && pix.load( url ) )
    {
        pix = pix.scaled( minimumWidth(), minimumHeight(),
                          Qt::KeepAspectRatioByExpanding,
                          Qt::SmoothTransformation );
    }
    else
    {
        pix = QPixmap( ":/noart.png" );
    }
    setPixmap( pix );
}

void CoverArtLabel::showArtUpdate( input_item_t *_p_item )
{
	LOGT(__FILE__);

    /* not for me */
    if ( _p_item != p_item )
        return;

    QString url;
    if ( _p_item ) url = THEMIM->getIM()->decodeArtURL( _p_item );
    showArtUpdate( url );
}

void CoverArtLabel::askForUpdate()
{
	LOGT(__FILE__);

    THEMIM->getIM()->requestArtUpdate( p_item );
}

void CoverArtLabel::setArtFromFile()
{
	LOGT(__FILE__);

    if( !p_item )
        return;

    QString filePath = QFileDialog::getOpenFileName( this, qtr( "Choose Cover Art" ),
        p_intf->p_sys->filepath, qtr( "Image Files (*.gif *.jpg *.jpeg *.png)" ) );

    if( filePath.isEmpty() )
        return;

    QString fileUrl = QUrl::fromLocalFile( filePath ).toString();

    THEMIM->getIM()->setArt( p_item, fileUrl );
}

void CoverArtLabel::clear()
{
	LOGT(__FILE__);

    showArtUpdate( "" );
}

TimeLabel::TimeLabel( intf_thread_t *_p_intf, TimeLabel::Display _displayType  )
    : ClickableQLabel(), p_intf( _p_intf ), bufTimer( new QTimer(this) ),
      buffering( false ), showBuffering(false), bufVal( -1 ), displayType( _displayType )
{
	LOGT(__FILE__);

    b_remainingTime = false;
    if( _displayType != TimeLabel::Elapsed )
        b_remainingTime = getSettings()->value( "MainWindow/ShowRemainingTime", false ).toBool();
    switch( _displayType ) {
        case TimeLabel::Elapsed:
            setText( " --:-- " );
            setToolTip( qtr("Elapsed time") );
            break;
        case TimeLabel::Remaining:
            setText( " --:-- " );
            setToolTip( qtr("Total/Remaining time")
                        + QString("\n-")
                        + qtr("Click to toggle between total and remaining time")
                      );
            break;
        case TimeLabel::Both:
            setText( " --:--/--:-- " );
            setToolTip( QString( "- " )
                + qtr( "Click to toggle between elapsed and remaining time" )
                + QString( "\n- " )
                + qtr( "Double click to jump to a chosen time position" ) );
            break;
    }
    setAlignment( Qt::AlignRight | Qt::AlignVCenter );

    bufTimer->setSingleShot( true );

    CONNECT( THEMIM->getIM(), positionUpdated( float, int64_t, int ),
              this, setDisplayPosition( float, int64_t, int ) );
    CONNECT( THEMIM->getIM(), cachingChanged( float ),
              this, updateBuffering( float ) );
    CONNECT( bufTimer, timeout(), this, updateBuffering() );

    setStyleSheet( "padding-left: 4px; padding-right: 4px;" );
}

void TimeLabel::setDisplayPosition( float pos, int64_t t, int length )
{
	LOGT(__FILE__);

    showBuffering = false;
    bufTimer->stop();

    if( pos == -1.f )
    {
        setMinimumSize( QSize( 0, 0 ) );
        if( displayType == TimeLabel::Both )
            setText( "--:--/--:--" );
        else
            setText( "--:--" );
        return;
    }

    int time = t / 1000000;

    secstotimestr( psz_length, length );
    secstotimestr( psz_time, ( b_remainingTime && length ) ? length - time
                                                           : time );

    // compute the minimum size that will be required for the psz_length
    // and use it to enforce a minimal size to avoid "dancing" widgets
    QSize minsize( 0, 0 );
    if ( length > 0 )
    {
        QMargins margins = contentsMargins();
        minsize += QSize(
                  fontMetrics().size( 0, QString( psz_length ), 0, 0 ).width(),
                  sizeHint().height()
                );
        minsize += QSize( margins.left() + margins.right() + 8, 0 ); /* +padding */

        if ( b_remainingTime )
            minsize += QSize( fontMetrics().size( 0, "-", 0, 0 ).width(), 0 );
    }

    switch( displayType )
    {
        case TimeLabel::Elapsed:
            setMinimumSize( minsize );
            setText( QString( psz_time ) );
            break;
        case TimeLabel::Remaining:
            if( b_remainingTime )
            {
                setMinimumSize( minsize );
                setText( QString("-") + QString( psz_time ) );
            }
            else
            {
                setMinimumSize( QSize( 0, 0 ) );
                setText( QString( psz_length ) );
            }
            break;
        case TimeLabel::Both:
        default:
            QString timestr = QString( "%1%2/%3" )
            .arg( QString( (b_remainingTime && length) ? "-" : "" ) )
            .arg( QString( psz_time ) )
            .arg( QString( ( !length && time ) ? "--:--" : psz_length ) );

            setText( timestr );
            break;
    }
    cachedLength = length;
}

void TimeLabel::setDisplayPosition( float pos )
{
	LOGT(__FILE__);

    if( pos == -1.f || cachedLength == 0 )
    {
        setText( " --:--/--:-- " );
        return;
    }

    int time = pos * cachedLength;
    secstotimestr( psz_time,
                   ( b_remainingTime && cachedLength ?
                   cachedLength - time : time ) );
    QString timestr = QString( "%1%2/%3" )
        .arg( QString( (b_remainingTime && cachedLength) ? "-" : "" ) )
        .arg( QString( psz_time ) )
        .arg( QString( ( !cachedLength && time ) ? "--:--" : psz_length ) );

    setText( timestr );
}


void TimeLabel::toggleTimeDisplay()
{
	LOGT(__FILE__);

    b_remainingTime = !b_remainingTime;
    getSettings()->setValue( "MainWindow/ShowRemainingTime", b_remainingTime );
}


void TimeLabel::updateBuffering( float _buffered )
{
	LOGT(__FILE__);

    bufVal = _buffered;
    if( !buffering || bufVal == 0 )
    {
        showBuffering = false;
        buffering = true;
        bufTimer->start(200);
    }
    else if( bufVal == 1 )
    {
        showBuffering = buffering = false;
        bufTimer->stop();
    }
    update();
}

void TimeLabel::updateBuffering()
{
	LOGT(__FILE__);

    showBuffering = true;
    update();
}

void TimeLabel::paintEvent( QPaintEvent* event )
{
	LOGT(__FILE__);

    if( showBuffering )
    {
        QRect r( rect() );
        r.setLeft( r.width() * bufVal );
        QPainter p( this );
        p.setOpacity( 0.4 );
        p.fillRect( r, palette().color( QPalette::Highlight ) );
    }
    QLabel::paintEvent( event );
}
