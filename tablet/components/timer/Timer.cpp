#include "Timer.h"
#include "stdio.h"

#define DEBUG 0
#define INFO 0
#define TRACE 0
#include "tablet_logger.h"

Timer::Timer(std::string name)
:mElapsed(0.)
,mDelta(0.)
{
}

double Timer::GetElapsed()
{
	return mElapsed;
}

double Timer::GetDelta()
{
	return mDelta;
}

void Timer::CountElapsed(double t)
{
	mElapsed += t;
}

void Timer::ResetElapsed()
{
	mElapsed = 0.;
}

void Timer::SetDelta(double t)
{
	mDelta = t;
}

int Timer::Reset()
{
	mElapsed = 0.;
	mDelta = 0.;
}

int Timer::Log()
{
	LOGD("mElapsed:%f\n", mElapsed);
	LOGD("mDelta:%f\n", mDelta);

	return 0;
}
