#ifndef TimerH
#define TimerH

#include <string>

class Timer
{
public:
	Timer(std::string name);

	double GetElapsed();
	double GetDelta();
	virtual int Update() = 0;
	virtual int Reset() = 0;
	void ResetElapsed();
	int Log();

protected:
	void CountElapsed(double t);
	void SetDelta(double t);

private:
	double mElapsed;
	double mDelta;
};

#endif
