#ifndef PosixTimerH
#define PosixTimerH

#include <time.h>
#include "Timer.h"
#include <string>

class PosixTimer: public Timer
{
public:
	PosixTimer();
	virtual int Update();
	virtual int Reset();

	int Log();

private:
	timespec old_t;
};

#endif
