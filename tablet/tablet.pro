QT += gui declarative 

CONFIG += console

DESTDIR = bin
OBJECTS_DIR = obj
MOC_DIR = moc
RCC_DIR = rcc
UI_DIR = ui
LIB_DIR = ./lib
RESOURCES = tablet.qrc

TEMPLATE = lib

INCLUDEPATH += /usr/local/include

LIBS += /usr/local/lib/libvlc.so

HEADERS     = 	tablet.hpp \
				recents.hpp \
				menus.hpp \
				main_interface.hpp \
              	input_manager.hpp \
              	extensions_manager.hpp \
              	dialogs_provider.hpp \
              	actions_manager.hpp \
              	util/timetooltip.hpp \
              	util/singleton.hpp \
              	util/searchlineedit.hpp \
              	util/registry.hpp \
              	util/qvlcframe.hpp \
              	util/qvlcapp.hpp \
              	util/qt_dirs.hpp \
              	util/qmenuview.hpp \
              	util/pictureflow.hpp \
              	util/input_slider.hpp \
              	util/customwidgets.hpp \
              
SOURCES     = widgets/video_widget.cpp \
			  Timer.cpp \
			  PosixTimer.cpp \
			  widgets/control_widget.cpp \
			  /widgets/QImageWidget.cpp \
			  main.cpp			 
