#include "dialogs/extended_tool.hpp"
#include "components/extended_panels.hpp"

#include <QFont>
#include <QVBoxLayout>
#include <QToolBar>
#include <QToolButton>
#include <QFont>
#include <QPushButton>
#include <QResizeEvent>
#include <QScrollArea>

#include "stdio.h"
#include "math.h"
#include "tablet.hpp"
#include "vlc_interface.h"
#include <string>

using namespace std;

#define DEBUG 0
#define INFO 0
#define TRACE 0
#include "tablet_logger.h"

QToolButton* createToolButton(QToolBar* topBar, QFont font, QString text, QString iconPath, QWidget* panel, bool addSeparator)
{
	LOGT(__FILE__);

    QToolButton* empty = new ExtendedToolButton(panel, 0);
    empty->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    empty->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    empty->setFont(font);

    empty->setText(text);
    topBar->addWidget(empty);
    empty->setIcon(QIcon(iconPath));

    if(addSeparator)
    {
    	topBar->addSeparator();
    }

    return empty;
}

ExtendedTool::ExtendedTool(intf_thread_t *_p_intf, QWidget* parent)
:QWidget(parent)
,p_intf(_p_intf)
,currentAudioPanel(0)
{
	LOGT(__FILE__);

	currentButton = 0;

    QVBoxLayout* audioSettingsLayout = new QVBoxLayout(this);
    audioSettingsLayout->setSpacing(0);
    audioSettingsLayout->setMargin(0);
	setLayout(audioSettingsLayout);

	//------------bar
	topBar = new QToolBar(this);
	topBar->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
    topBar->setMovable(false);
//	topBar->setFrameStyle( QFrame::NoFrame );
    topBar->setStyleSheet("background-color:rgb(255, 255, 255, 0);");

    //----------------panel
    QWidget* audioPanel = new QWidget(this);
    audioPanelLayout = new QVBoxLayout(audioPanel);
    audioPanelLayout->setMargin(0);
    audioPanelLayout->setSpacing(0);
    audioPanel->setStyleSheet("background-color:rgb(255, 255, 255, 0);");

    QScrollArea *scroll = new QScrollArea;
    scroll->setWidgetResizable( true );
    scroll->setWidget( audioPanel );

    audioSettingsLayout->addWidget(topBar, 1);
    audioSettingsLayout->addWidget(scroll, 10);
}

int ExtendedTool::addButton(QString text, QString iconPath, QWidget* panel, bool addSeparator)
{
	QAbstractButton* button = createToolButton(topBar, mFont, text, iconPath, panel, addSeparator);
    mButtons.push_back(button);
    CONNECT( button, clicked(), this, onToolClick() );

		LOGD("i_font_size:%d\n", i_font_size);

	 QFont f = font();

    if(panel)
    {
    	panel->hide();
    }

    if(button)
    {
    	button->setFont(f);
    }

    if(!currentButton)
    {
    	switchTool(button);
    }

	return 0;
}

int ExtendedTool::switchTool(QObject* sender)
{
	LOGT(__FILE__);

	if(currentButton)
	{
		currentButton->setStyleSheet("background-color:rgb(255, 255, 255, 0);");
	}

	currentButton = (ExtendedToolButton*)sender;
	currentButton->setStyleSheet(     "background: qradialgradient(cx:0, cy:0, radius: 1,"
            "fx:1.0, fy:1.0, stop:0 rgba(255,255,255, 150), stop:1 rgba(255,255,255, 150));"
 "border-radius: 4px;"
"border-style: outset;"
"border-width: 2px;"
"border-color: rgba(100,100,255, 150);");

	LOGD("button:%p\n", currentButton);
	LOGD("currentAudioPanel:%p\n", currentAudioPanel);

	if(currentAudioPanel)
	{
		audioPanelLayout->removeWidget(currentAudioPanel);
		currentAudioPanel->hide();
		currentAudioPanel = 0;
	}

	if(currentButton->mPanel)
	{
		audioPanelLayout->addWidget(currentButton->mPanel);
		currentAudioPanel = currentButton->mPanel;
		currentAudioPanel->show();
	}

	return 0;
}

void ExtendedTool::onToolClick()
{
	LOGT(__FILE__);

	switchTool(sender());
}
