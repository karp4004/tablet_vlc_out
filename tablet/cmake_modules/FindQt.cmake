# Locate QT library
# This module defines QT_FOUND, QT_INCLUDE_DIR and QT_LIBRARIES standard variables

set(QT_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	/usr/include
	/usr/share/qt4/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(QT_INCLUDE_DIR
	NAMES QtGui QtDeclarative
	HINTS
	$ENV{QT_DIR}
	${QT_DIR}
	PATH_SUFFIXES QtGui QtDeclarative
	PATHS ${QT_SEARCH_PATHS}
)

find_library(QT_LIBRARY
	NAMES ev
	HINTS
	$ENV{QT_DIR}
	${QT_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${QT_SEARCH_PATHS}
)
	
if(QT_LIBRARY)
    set(QT_LIBRARIES "${QT_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set QT_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(QT DEFAULT_MSG QT_LIBRARIES QT_INCLUDE_DIR)
