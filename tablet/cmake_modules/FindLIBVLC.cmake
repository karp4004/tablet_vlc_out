# Locate LBVLC library
# This module defines LBVLC_FOUND, LBVLC_INCLUDE_DIR and LBVLC_LIBRARIES standard variables

set(LBVLC_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LBVLC_INCLUDE_DIR
	NAMES vlc.h
	HINTS
	$ENV{LBVLC_DIR}
	${LBVLC_DIR}
	PATH_SUFFIXES vlc
	PATHS ${LBVLC_SEARCH_PATHS}
)

find_library(LBVLC_LIBRARY
	NAMES vlc
	HINTS
	$ENV{LBVLC_DIR}
	${LBVLC_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LBVLC_SEARCH_PATHS}
)
	
if(LBVLC_LIBRARY)
    set(LBVLC_LIBRARIES "${LBVLC_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LBVLC_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LBVLC DEFAULT_MSG LBVLC_LIBRARIES LBVLC_INCLUDE_DIR)
