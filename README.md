# This is my README

1. Compilation, installation.
   - set CMAKE_INSTALL_PREFIX
     (<root>/tablet/CMakeLists.txt)
   - cd <root>/tablet
   - mkdir build
   - cd build
   - cmake ..
   - make
   - sudo make install
   
2. Uninstall
   - sudo rm -rf CMAKE_INSTALL_PREFIX/share/qml
   - sudo rm -rf CMAKE_INSTALL_PREFIX/lib/vlc/plugins/gui/libtablet_plugin.so